import pygame
import sys
import Sprites
import random

class Gtest1(object):
    def __init__(self):
        pygame.init()
        self.window = pygame.display.set_mode((600,600)) 
        pygame.display.set_caption('Falling')
        self.screen = pygame.display.get_surface()
        self.clock = pygame.time.Clock()
        
    def run(self):
        self.background = pygame.Surface(self.screen.get_size())
        self.background.fill((0,0,0))
        self.player = Sprites.PlayerSprite()
        self.player.set_pos([100,100])
        self.player.set_friction(0.98)
        self.pgroup = pygame.sprite.GroupSingle()
        self.pgroup.add(self.player)
        self.rain = self.make_rain(200)
        self.rocks = self.make_rocks(100)
        while True:
            self.player.update()
            self.clock.tick(60)
            self.screen.blit(self.background, (0,0))
            self.rain.update()
            self.rocks.update()
            self.pgroup.draw(self.screen)
            self.rain.draw(self.screen)
            self.rocks.draw(self.screen)
            pygame.display.flip() #Flips it all the the screen
            self.handle_events()
            self.handle_keys()
            
            if pygame.sprite.spritecollideany(self.player , self.rocks): 
                GameOverScreen(self.screen, self.clock).run()
            
    def handle_keys(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RIGHT]:
            self.player.add_vel((0.1, 0))
        if keys[pygame.K_LEFT]:
            self.player.add_vel((-0.1, 0))
        if keys[pygame.K_UP]:
            self.player.add_vel((0, -0.1))
        if keys[pygame.K_DOWN]:
            self.player.add_vel((0,0.1))
            
            
    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                print "Exiting!"
                sys.exit(0) 
        
    def make_rain(self, number):
        group = pygame.sprite.Group()
        for i in range(number):
            group.add(Sprites.RainSprite(0,600,600))
        return group
    
    def make_rocks(self, number):
        group = pygame.sprite.Group()
        for i in range(number):
            group.add(Sprites.RockSprite(0,600,600))
        return group
    
class Screen(object):
    def __init__(self, screen, clock):
        self.screen = screen
        self.clock = clock
    def run(self):
        pass
    def quit_if_quitting(self):
        if pygame.event.peek(pygame.QUIT):
            print "Quitting"

            sys.exit(0)
            
class GameOverScreen(Screen):
    def run(self):
        print "GameOverScreen run"
        font = pygame.font.Font(None, 100)
        text = font.render("Game Over", 1, (255,255,255))
        self.background = pygame.Surface(self.screen.get_size())
        self.background.fill((255,0,0))
        self.background.blit(text, (50,self.screen.get_size()[1]/2))
        while 1:
            self.clock.tick(60)
            self.quit_if_quitting()
            self.screen.blit(self.background, (0,0))
            pygame.display.flip()
        
Gtest1().run()
        
    