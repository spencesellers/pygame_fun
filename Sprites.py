import pygame
import random
class BaseSprite(pygame.sprite.Sprite):
    def __init__(self, size, color):
        super(BaseSprite, self).__init__()
        self.pos = [0,0]
        self.size = size
        self.color = color
        self.image = pygame.Surface(size)
        self.image.fill(color)
        self.rect = self.image.get_rect()
        
    def set_pos(self, pos):
        self.pos = pos
        self.rect.topleft = (pos[0], pos[1])
    def move(self, rel_pos):
        self.set_pos( (self.pos[0] + rel_pos[0], self.pos[1] + rel_pos[1]))
        
    def set_size(self, size):
        self.image = pygame.Surface(size)
        self.rect = self.image.get_rect()
    def set_color(self, color):
        self.image.fill(color)
    def update(self):
        pass
    
class VelSprite(BaseSprite):
    def __init__(self, size, color):
        super(VelSprite, self).__init__( size, color)
        self.vel = [0,0]
        self.friction = 1
        self.gravity = 0
    def add_vel(self, dv):
        self.vel = (self.vel[0] + dv[0], self.vel[1] + dv[1])
    def set_friction(self, friction):
        self.friction = friction
    def update(self):
        super(VelSprite, self).update()
        self.move(self.vel)
        self.vel = [self.friction * self.vel[0], self.friction * self.vel[1]]
        self.vel[1] += self.gravity

class PlayerSprite(VelSprite):
    def __init__(self):
        super(PlayerSprite, self).__init__((10,10), (255,0,0))
        self.maxrange = pygame.display.get_surface().get_size()
    def update(self):
        super(PlayerSprite, self).update()
        
class RainSprite(VelSprite):
    def __init__(self, top, bottom, range):
        size = (5,5)
        color = (0,0,random.randint(100,255))
        super(RainSprite, self).__init__( size, color)
        
        self.friction = 1
        self.gravity = 0.01
        self.top = top
        self.bottom = bottom
        self.range = range
        self.return_to_top(height = random.randint(top, bottom))
        self.image = pygame.image.load("rain.png")

    def update(self):
        super(RainSprite, self).update()
        if self.pos[1] >= self.bottom:
            self.return_to_top()
            
    def return_to_top(self, height = None):
        if height == None:
            height = self.top
        self.pos = [random.randint(0,self.range), height]
        self.vel = [0,0]
        self.vel[1] += random.random() - 0.5
        
class RockSprite(RainSprite):
    def __init__(self, top, bottom, range):
        super(RockSprite, self).__init__( top, bottom, range)
        self.image = pygame.image.load("rock.png")
    def return_to_top(self, height = None):
        super(RockSprite, self).return_to_top(height)
        
        